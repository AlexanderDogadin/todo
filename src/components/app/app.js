import React, {Component} from 'react'
import AppHeader from '../app-header'
import SearchPanel from '../search-panel'
import TodoList from '../todo-list'
import ItemStatusFilter from "../item-status-filter"
import ItemAddForm from "../item-add-form";
import './app.css'

export default class App extends Component {

    state = {
        todoData: [
            this.createTodoItem('Learn React', 1),
            this.createTodoItem('Create App', 2),
            this.createTodoItem('Modify App', 3),
        ],
        searchString: '',
        searchDone: 'none'
    }

    createTodoItem(text, id) {
        //generate id
        const {state = {}} = this
        const {todoData = []} = state
        if (id === undefined) {
            id = todoData.reduce((a, b) => a.id < b.id ? b : a
                , {id: 0}).id + 1
        }
        return {
            item: text,
            important: false,
            done: false,
            id
        }
    }

    deleteItem = (id) => {
        this.setState(({todoData}) => {
            const ind = todoData.findIndex((el) => el.id === id)
            const ret = [
                ...todoData.slice(0, ind),
                ...todoData.slice(ind + 1)
            ]
            return {todoData: ret}
        })
    }

    addItem = (text) => {
        //add element in array
        const newItem = this.createTodoItem(text)
        this.setState(({todoData}) => {
            const ret = [
                ...todoData,
                newItem
            ]
            return {todoData: ret}
        })
    }

    toggleProperty(arr, id, propName) {
        const ind = arr.findIndex((el) => el.id === id)
        const oldItem = arr[ind]
        const newItem = {...oldItem, [propName]: !oldItem[propName]}
        const ret = [
            ...arr.slice(0, ind),
            newItem,
            ...arr.slice(ind + 1)
        ]
        return ret
    }

    onToggleImportant = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, 'important')
            }
        })
    }

    onToggleDone = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, 'done')
            }
        })
    }

    onSearch = (event) => {
        const searchString = event.target.value
        this.setState({searchString})
    }

    onSearchDone = (event) => {
        let done = ''
        done = event.target.getAttribute('data-done')
        if (done !== '' && done !== undefined) {
            this.setState({searchDone: done})
        }
    }

    onFiltered(items){
        const {searchString,searchDone} = this.state
        let ret = [...items]
        const searchStr = searchString.toLowerCase()
        if (searchString !== ''){
            ret = ret.filter(({item}) => item.toLowerCase().indexOf(searchStr) !== -1)
        }
        if (searchDone !== 'none') {
            const searchD = searchDone === 'true'
            ret = ret.filter(({done}) => done === searchD)
        }
        return ret
    }

    render() {
        const {todoData, searchDone} = this.state

        const doneCount = todoData
            .filter((item) => item.done).length
        const todoCount = todoData.length - doneCount

        const filteredData = this.onFiltered(todoData)

        return (
            <div className="todo-app">
                <AppHeader toDo={todoCount} done={doneCount}/>
                <div className="top-panel d-flex">
                    <SearchPanel onSearch={this.onSearch}/>
                    <ItemStatusFilter
                        onSearchDone={this.onSearchDone}
                        actionBtn ={searchDone}/>
                </div>
                <TodoList
                    items={filteredData}
                    onDeleted={this.deleteItem}
                    onToggleImportant={this.onToggleImportant}
                    onToggleDone={this.onToggleDone}
                />

                <ItemAddForm onItemAdded={this.addItem}/>
            </div>
        )
    }
}