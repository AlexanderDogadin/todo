import React from 'react'
import './search-panel.css'

const SearchPanel = ({onSearch}) => {
    const searchText = 'Type here to search'

    return (
        <input
            type='text'
            className='form-control search-input'
            onChange={onSearch}
            placeholder={searchText}/>
    )
}
export default SearchPanel