import React, {Component} from 'react';

import './item-status-filter.css';

export default class ItemStatusFilter extends Component {

    buttonsType = [
        {name: 'All', dataDone: 'none'},
        {name: 'Active', dataDone: 'false'},
        {name: 'Done', dataDone: 'true'}
    ]

    buttonList() {
        const {onSearchDone, actionBtn} = this.props
        return this.buttonsType.map(({name, dataDone}) => {
            const className = dataDone === actionBtn ? 'btn btn-info' : 'btn btn-outline-secondary'
            return (
                <button type="button"
                        key = {name}
                        data-done={dataDone}
                        onClick={onSearchDone}
                        className={className}>
                    {name}
                </button>
            )
        })
    }

    render() {

        const buttons = this.buttonList()

        return (
            <div className="btn-group">
                {buttons}
            </div>
        );
    }
}
